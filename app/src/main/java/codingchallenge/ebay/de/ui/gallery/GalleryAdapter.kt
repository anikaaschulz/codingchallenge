package codingchallenge.ebay.de.ui.gallery

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import codingchallenge.ebay.de.App
import codingchallenge.ebay.de.R
import codingchallenge.ebay.de.databinding.GalleryItemBinding
import codingchallenge.ebay.de.model.CarImage
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

/**
 * Adapter for a RecyclerView holding car images.
 */
class GalleryAdapter : RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {
    var items: List<CarImage> = listOf()
        set(v) {
            field = v.toList()
            notifyDataSetChanged()
        }
    private val imageClickedSubject = PublishSubject.create<CarImage>()

    //A ViewHolder holding a Binding for displaying an image
    class ViewHolder(val binding: GalleryItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = GalleryItemBinding.inflate(LayoutInflater.from(parent.context))
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        App.instance.picasso
                .load(items.get(position)
                .getThumbnailUrl())
                .placeholder(R.drawable.ic_placeholder_image)
                .into(holder.binding.image)
        holder.binding.image.setOnClickListener({ _ -> imageClickedSubject.onNext(items[position]) })
    }

    override fun getItemCount(): Int {
        return items.size
    }

    //subscribe on this Observable for being notified when the user clicked on an image
    fun onImageClicked() : Observable<CarImage> {
        return imageClickedSubject.subscribeOn(Schedulers.io())
    }
}

