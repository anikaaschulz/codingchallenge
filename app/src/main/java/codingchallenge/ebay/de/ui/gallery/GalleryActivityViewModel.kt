package codingchallenge.ebay.de.ui.gallery
import android.arch.lifecycle.ViewModel
import codingchallenge.ebay.de.App
import codingchallenge.ebay.de.model.CarImage
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * ViewModel that holds car images
 * displayed by a GalleryActivity.
 */
class GalleryActivityViewModel : ViewModel() {
    private var carImages : List<CarImage> = listOf()

    fun getCarImages() : Single<List<CarImage>> {
        if (carImages.isNotEmpty()) {
            return Single.just(carImages)
        }

        return App.instance.dataRepository.getCarImages()
                .observeOn(AndroidSchedulers.mainThread())
                .map { carImages ->
                    this.carImages = carImages
                    carImages
                 }
    }

}