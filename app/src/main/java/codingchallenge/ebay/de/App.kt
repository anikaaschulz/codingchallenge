package codingchallenge.ebay.de

import android.app.Application
import codingchallenge.ebay.de.network.ApiClient
import codingchallenge.ebay.de.data.DataRepository
import com.squareup.picasso.Picasso

class App: Application() {
    val picasso: Picasso by lazy { Picasso.with(this) }
    val dataRepository by lazy { DataRepository(ApiClient(this)) }

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }
}