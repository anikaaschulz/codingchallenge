package codingchallenge.ebay.de.model

/**
 * A simple model containing the list of
 * car images displayed in the app.
 * The data retrieved from the api contains
 * a lot more information than this model which
 * could be added later when needed.
 */
data class Car(
    val images: List<CarImage>
)