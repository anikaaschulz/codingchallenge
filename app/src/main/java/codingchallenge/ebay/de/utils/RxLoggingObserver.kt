package codingchallenge.ebay.de.utils

import android.util.Log
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver

/**
 * A simple DisposableObserver that logs rx events
 * and can be easily added to a CompositeDisposable
 * by calling "addTo" function.
 */
class RxLoggingObserver<T>: DisposableObserver<T>(), SingleObserver<T> {
    private val TAG = RxLoggingObserver::class.java.simpleName

    override fun onSuccess(t: T) {
        Log.d(TAG, "onSuccess")
    }

    override fun onNext(t: T) {
        Log.d(TAG, "onNext")
    }

    override fun onError(e: Throwable) {
        Log.e(TAG, "onError: " + e.message)
    }

    override fun onComplete() {
        Log.d(TAG, "onComplete")
    }

    fun addTo(compositeDisposable: CompositeDisposable) {
        compositeDisposable.add(this)
    }
}
