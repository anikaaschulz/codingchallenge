package codingchallenge.ebay.de.ui.fullscreenimage

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import codingchallenge.ebay.de.App
import codingchallenge.ebay.de.R
import codingchallenge.ebay.de.databinding.ActivityFullscreenImageBinding
import com.squareup.picasso.Callback

/**
 * Activity that displays an image in fullscreen mode.
 * Given a url for a full size image and an url for
 * a thumbnail, it first displays a thumbnail which
 * is replaced by the full size image if loading was finished.
 */
class FullScreenImageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFullscreenImageBinding

    companion object {
        private val KEY_URL = "url"
        private val KEY_THUMBNAIL_URL = "thumbnail"

        fun newIntent(context: Context, url: String, thumbnailUrl: String): Intent {
            val intent = Intent(context, FullScreenImageActivity::class.java)
            intent.putExtra(KEY_URL, url)
            intent.putExtra(KEY_THUMBNAIL_URL, thumbnailUrl)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFullscreenImageBinding.inflate(layoutInflater, null, false)
        setContentView(binding.root)
        initToolbar()
        setImage(intent.getStringExtra(KEY_URL), intent.getStringExtra(KEY_THUMBNAIL_URL))
    }

    private fun initToolbar(){
        supportActionBar?.setTitle(R.string.activity_fullscreen_image_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setImage(url: String, thumbnailUrl: String) {
        App.instance.picasso
                .load(thumbnailUrl) // load thumbnail first
                .placeholder(R.drawable.ic_placeholder_image)
                .into(binding.image, object : Callback {
                    //on success of loading thumbnail, load large image
                    override fun onSuccess() {
                        App.instance.picasso
                                .load(url)
                                .placeholder(binding.image.drawable)
                                .into(binding.image)
                    }

                    override fun onError() {}
                })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}