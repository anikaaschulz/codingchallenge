package codingchallenge.ebay.de.ui.gallery

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import codingchallenge.ebay.de.R
import codingchallenge.ebay.de.databinding.ActivityGalleryBinding
import codingchallenge.ebay.de.model.CarImage
import codingchallenge.ebay.de.ui.fullscreenimage.FullScreenImageActivity
import codingchallenge.ebay.de.utils.RxLoggingObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Activity that after app start fetches all images for the car from the backend and
 * displays them in a grid.
 */
class GalleryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGalleryBinding
    private val viewModel: GalleryActivityViewModel by lazy { ViewModelProviders.of(this).get(GalleryActivityViewModel::class.java) }
    private val galleryAdapter = GalleryAdapter()
    private val compositeDisposable = CompositeDisposable();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGalleryBinding.inflate(layoutInflater, null, false)
        setContentView(binding.root)
        initGallery()
        loadData()
    }

    private fun initGallery(){
        binding.gallery.layoutManager = GridLayoutManager(this, 2)
        binding.gallery.adapter = galleryAdapter
        galleryAdapter.onImageClicked()
                .observeOn(Schedulers.io())
                .doOnNext({ carImage ->
                    startActivity(FullScreenImageActivity.newIntent(this@GalleryActivity,
                            carImage.getFullSizeUrl(), carImage.getThumbnailUrl()))
                }).subscribeWith(RxLoggingObserver<CarImage>())
                .addTo(compositeDisposable)
    }

    private fun loadData(){
        binding.progress.visibility = View.VISIBLE
        viewModel.getCarImages()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { _ -> showConnectionErrorSnackbar() }
                .doOnSuccess { carImages -> galleryAdapter.items = carImages }
                .doFinally{ binding.progress.visibility = View.GONE}
                .subscribeWith(RxLoggingObserver<List<CarImage>>())
                .addTo(compositeDisposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    private fun showConnectionErrorSnackbar(){
        Snackbar.make(binding.root, R.string.gallery_connection_error, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.gallery_connection_error_retry) { loadData() }
                .show()
    }
}