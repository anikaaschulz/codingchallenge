package codingchallenge.ebay.de.data

import codingchallenge.ebay.de.model.Car
import codingchallenge.ebay.de.model.CarImage
import codingchallenge.ebay.de.network.ApiClient
import com.google.gson.Gson
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 * Repository that knows how
 * to load car images from the api.
 */
class DataRepository(private val apiClient: ApiClient) {

    private val url = "https://m.mobile.de/svc/a/246285424"

    fun getCarImages() : Single<List<CarImage>> {
        return apiClient.downloadData(url)
                .observeOn(Schedulers.io())
                .map { json -> Gson().fromJson<Car>(json, Car::class.java).images }
                .retry(2)
    }
}
