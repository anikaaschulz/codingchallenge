package codingchallenge.ebay.de.network

import codingchallenge.ebay.de.App
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.File

/**
 * A simple Api client using OkHttp.
 * It is able to fetch data from a specified
 * url asynchronously and return it in a Single.
 */
class ApiClient(app: App) {
    private val okHttpClient: OkHttpClient

    init {
        okHttpClient = initOkHttpClient(app)
    }

    private fun initOkHttpClient(app: App): OkHttpClient {
        val cacheDir = File(app.cacheDir, "http")
        val cache = Cache(cacheDir, 10 * 1024 * 1024) // 10 MiB
        return OkHttpClient.Builder()
                .cache(cache)
                .build()
    }

    /**
     * Returns a Single, that, given a URL, sets up a connection and gets the HTTP response body from the server.
     * If the network request is successful, it returns the response body in String form. Otherwise,
     * it will throw an IOException. The returned Single is automatically subscribed on io thread.
     */
    fun downloadData(url: String): Single<String> {
        return Single.fromCallable {
            val request = Request.Builder()
                    .url(url)
                    .build()

            val response = okHttpClient.newCall(request).execute()
            val result : String = response.body()?.string() ?: ""
            result
        }.subscribeOn(Schedulers.io())
    }
}
