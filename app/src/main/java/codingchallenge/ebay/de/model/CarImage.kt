package codingchallenge.ebay.de.model

/**
 * Represents data for a single image of a car.
 */
class CarImage(private val uri: String, val set: String) {

    fun getThumbnailUrl() : String {
        return "https://${uri}_2.jpg"
    }

    fun getFullSizeUrl() : String{
        return "https://${uri}_27.jpg"
    }
}